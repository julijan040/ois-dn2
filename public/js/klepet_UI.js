var swearwords = [];

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {



  return $('<div></div>').html('<i>' + sporocilo + '</i>'); //zacetek 2_3 veje razvoja!

}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {


    
    sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"/>'); 
    sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"/>'); 
    sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"/>'); 
    sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"/>'); 
    sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"/>'); 
    
    

    klepetApp.posljiSporocilo($('#kanal2').text(), sporocilo);


    
    sporocilo=kletvice(sporocilo);
    
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);

    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    
    
     
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  $.get("../public/swearWords.txt" , function( data ) {
      swearwords = data.split(/\r?\n/);
      
  });
  
  
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      
      
      $('#uporabnik').text(rezultat.vzdevek + ' @ ');
      
      
      
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal2').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function kletvice (sporocilo){
  for(var i=0; i<swearwords.length; i++) {
    var stzvezd = "";
    for(var y=0; y<swearwords[i].length; y++) {
      stzvezd+="*";
      
    }
      var reg = new RegExp(swearwords[i], 'gi');
     sporocilo = sporocilo.replace(reg, stzvezd);
  }
  
  return sporocilo;
}
